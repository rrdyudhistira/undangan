import type { CustomAppPage } from 'next/app'
import '../styles/index.css'

const MyApp: CustomAppPage = ({ Component, pageProps }) => {
  return <Component {...pageProps} />
}

export default MyApp

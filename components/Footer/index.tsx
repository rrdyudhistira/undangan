export default function Footer() {
  return (
    <div className="relative flex justify-center pt-2 pb-2 pl-4 m-auto bg-pastelgreen shadow-lg bg-fixed">
        <p>© 2022 M. Rivaldi Irawan, Copyright Website</p>
    </div>
  )
}
